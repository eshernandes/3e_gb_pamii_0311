import { BuscasPopularesComponent } from './buscas-populares/buscas-populares.component';
import { CardsComponent } from './cards/cards.component';
import { InformacoesComponent } from './informacoes/informacoes.component';
import { CarouselComponent } from './carousel/carousel.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SwiperModule } from 'swiper/angular';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    SwiperModule
  ],
  declarations: [
    HomePage,
    CarouselComponent,
    InformacoesComponent,
    CardsComponent,
    BuscasPopularesComponent
  ]
})
export class HomePageModule {}
