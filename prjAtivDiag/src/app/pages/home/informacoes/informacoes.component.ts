import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-informacoes',
  templateUrl: './informacoes.component.html',
  styleUrls: ['./informacoes.component.scss'],
})
export class InformacoesComponent implements OnInit {

  informacoes = [
    {
      id: 0,
      img: '../../../../assets/images/inf1.webp',
      title: 'O que preciso para comprar?',
      button: 'Guia de compra'
    },
    {
      id: 1,
      img: '../../../../assets/images/inf2.webp',
      title: 'O que preciso para alugar?',
      button: 'Guia de aluguel'
    }
  ];
  constructor() { }

  ngOnInit() {}

}
