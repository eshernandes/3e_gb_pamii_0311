import { Component, OnInit, AfterContentChecked, ViewChild, ViewEncapsulation } from '@angular/core';
import { SwiperComponent } from 'swiper/angular';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarouselComponent implements OnInit,AfterContentChecked {
  @ViewChild('swiper') swiper: SwiperComponent;

  imoveis = [
    {
      id: 0,
      image: '../../../../assets/images/imovel1.jpg',
      price: 'R$ 650.000'
    },
    {
      id: 1,
      image: '../../../../assets/images/imovel2.jpg',
      price: 'R$ 4200/mês'
    },
    {
      id: 2,
      image: '../../../../assets/images/imovel3.jpg',
      price: 'R$ 360.000'
    }
  ];

  constructor() { }

  ngOnInit() {}
  ngAfterContentChecked(): void {
    if(this.swiper){
      this.swiper.updateSwiper({});
    }
  }

}
