import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buscas-populares',
  templateUrl: './buscas-populares.component.html',
  styleUrls: ['./buscas-populares.component.scss'],
})
export class BuscasPopularesComponent implements OnInit {

  textos = [
    {
      img: '../../../../assets/images/saopaulo.webp',
      apart: 'Apartamentos para alugar em São Paulo',
      casas: 'Casas à venda em São Paulo',
      kitnet: 'Kitnets para alugar em São Paulo'
    },
    {
      img: '../../../../assets/images/fortaleza.jpg',
      apart: 'Apartamentos para alugar em Fortaleza',
      casas: 'Casas à venda em Fortaleza',
      kitnet: 'Kitnets para alugar em Fortaleza'
    },
    {
      img: '../../../../assets/images/santos.jpg',
      apart: 'Apartamentos para alugar em Santos',
      casas: 'Casas à venda em Santos',
      kitnet: 'Kitnets para alugar em Santos'
    }
  ];

  slideOpts = {
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    }
  };
  constructor() { }

  ngOnInit() {}

}
